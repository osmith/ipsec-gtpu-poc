#!/bin/sh -e
# based on example from
# https://www.slideshare.net/kentaroebisawa/using-gtp-on-linux-with-libgtpnl

if [ "$(id -u)" != "0" ]; then
	echo "ERROR: run me as root!"
	exit 1
fi

set -x

start() {
	set -e

	modprobe gtp
	echo -n 'module gtp +p' > /sys/kernel/debug/dynamic_debug/control

	ip link add veth1 type veth peer name veth2
	ip addr add 172.0.0.1/24 dev veth1
	ip link set veth1 up
	ip addr add 172.99.0.1/32 dev lo

	gtp-link add gtp1 > /tmp/log-gtp-link1 2>&1 &
	gtp-tunnel add gtp1 v1 200 100 172.99.0.2 172.0.0.2
	ip route add 172.99.0.2/32 dev gtp1

	ip netns add ns2
	ip link set veth2 netns ns2
	ip netns exec ns2 ip addr add 172.0.0.2/24 dev veth2
	ip netns exec ns2 ip link set veth2 up
	ip netns exec ns2 ip addr add 172.99.0.2/32 dev lo
	ip netns exec ns2 ip link set lo up
	ip netns exec ns2 gtp-link add gtp2 > /tmp/log-gtp-link2 2>&1 &
	ip netns exec ns2 gtp-tunnel add gtp2 v1 100 200 172.99.0.1 172.0.0.1
	ip netns exec ns2 ip route add 172.99.0.1/32 dev gtp2

	gtp-tunnel list
	ip netns exec ns2 gtp-tunnel list
}

stop() {
	set +e
	ip addr del 172.99.0.1/32 dev lo
	ip link set veth1 down
	ip addr del 172.0.0.1/24 dev veth1
	ip link del veth1

	killall gtp-tunnel
	killall gtp-link

	ip route del 172.99.0.2/32 dev gtp1
	gtp-tunnel delete gtp1 v1 200
	gtp-link del gtp1

	ip netns del ns2
	gtp-tunnel delete gtp2 v1 100
	gtp-link del gtp2

	modprobe -r gtp
}


stop

set +x
echo
echo "--- start ---"
echo
set -x

start

tail -F /tmp/log-gtp-link1 /tmp/log-gtp-link2

set +x
echo
echo "--- stop ---"
echo
set -x

stop
