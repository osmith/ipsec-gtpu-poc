# ipsec-gtpu proof of concept

WIP scripts/configs used while researching for osmo-epdg, see https://osmocom.org/projects/osmo-epdg/wiki/EPDG_implementation_plan

|       | Server   | Client   |
|-------|----------|----------|
| lan   | 10.0.0.1 | 10.0.0.2 |
| ipsec | 10.1.0.1 | 10.1.0.2 |
| gtp   | 10.2.0.1 | 10.2.0.2 |


How to use:

* enable ip forward on server and client
* deploy configs (`/etc/swanctl/swanctl.conf`)
* server: copy server.network contents to /etc/systemd/network/
* client: copy client.network contents to /etc/systemd/network/
* client and server: start charon-systemd (debian: `systemctl start strongswan`)
* client: run `swanctl --initiate --child host-host`
* client: run `client.gtp.sh`
* server: run `server.gtp.sh`

The strongswan configs are somewhat based on
[this example](https://www.strongswan.org/testing/testresults/ikev2/rw-psk-ipv4/index.html)
and could be improved a lot, this is just a proof of concept.
