#!/bin/sh -e

if [ "$(id -u)" != "0" ]; then
	echo "ERROR: run me as root!"
	exit 1
fi

set -x

start() {
	set -e

	modprobe gtp
	echo -n 'module gtp +p' > /sys/kernel/debug/dynamic_debug/control

	ip addr add 10.2.0.2/32 dev lo

	gtp-link add gtp1 &
	sleep 0.5
	gtp-tunnel add gtp1 v1 100 200 10.2.0.1 10.1.0.1
	ip route add 10.2.0.1/32 dev gtp1

	gtp-tunnel list
}

stop() {
	set +e

	ip addr del 10.2.0.2/32 dev lo

	killall gtp-tunnel
	killall gtp-link

	ip route del 10.2.0.1/32 dev gtp1
	gtp-tunnel delete gtp1 v1 100
	gtp-link del gtp1

	set -e
	modprobe -r gtp
}


stop

set +x
echo
echo "--- start ---"
echo
set -x

start
